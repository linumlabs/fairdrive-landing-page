import React, { useContext } from "react";
import { ThemeContext } from "../../store/themeContext/themeContext";
import useStyles from "./footerStyles";
import { Twitter, Medium, Discord } from "../../components/icons/icons";
import { Link } from "react-router-dom";

import { ReactComponent as SwarmLogotypeBlackSVG } from "../../media/images/logotypes/swarm.svg";
import { ReactComponent as LinumLabsBlackSVG } from "../../media/images/logotypes/linum-labs.svg";
import { ReactComponent as FDSBlackSVG } from "../../media/images/logotypes/fds.svg";

export interface Props {}

function Footer(props: Props) {
  const { theme } = useContext(ThemeContext);
  const classes = useStyles({ ...props, ...theme });

  return (
    <footer className={classes.footer}>
      <div className={`${classes.column} ${classes.left}`}>
        <p>Made in collaboration with these ecosystem partners</p>
        <div className={classes.links}>
          <a
            rel="noopener noreferrer"
            target="_blank"
            href="https://www.ethswarm.org/"
          >
            <SwarmLogotypeBlackSVG className={classes.linkIcon} />
          </a>
          <a
            rel="noopener noreferrer"
            target="_blank"
            href="https://fairdatasociety.org/"
          >
            <FDSBlackSVG className={classes.linkIconFSB} />
          </a>

          <a
            rel="noopener noreferrer"
            target="_blank"
            href="https://linumlabs.com/"
          >
            <LinumLabsBlackSVG className={classes.linkIcon} />
          </a>
        </div>
        <Link className={classes.link} to="/terms-and-conditions">
          Terms & Conditions
        </Link>
      </div>
      <div className={`${classes.column} ${classes.right}`}>
        <p>Join the conversation:</p>
        <div className={classes.linksRight}>
          <a
            rel="noopener noreferrer"
            target="_blank"
            className={classes.linkItem}
            href="#"
          >
            <Discord className={classes.socialIcon} />
          </a>
          <a
            rel="noopener noreferrer"
            target="_blank"
            className={classes.linkItem}
            href="https://twitter.com/fairdatasociety"
          >
            <Twitter className={classes.socialIcon} />
          </a>
          <a
            rel="noopener noreferrer"
            target="_blank"
            className={classes.linkItem}
            href="https://medium.com/fair-data-society"
          >
            <Medium className={classes.socialIcon} />
          </a>
        </div>
      </div>
    </footer>
  );
}

export default React.memo(Footer);
